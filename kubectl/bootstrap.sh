#!/bin/bash
#
# Uses a shell-script file as an template. The variables present on the
# `config_template.yml.sh` will be evaluated against the current environment
# variables. The outcome is saved as `kubectl` primary configuration file.
#

eval "cat <<EOF
$(</etc/kubectl/config_template.yml.sh)
EOF
" 2> /dev/null > '/root/.kube/config'

helm repo update
